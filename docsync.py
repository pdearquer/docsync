##
# DocSync Repository Management Tool
# Copyright 2023 Pablo Arquer <pdearquer@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import re
import time
import fnmatch
import shutil
import errno

docDir = ".doc"
docName = os.path.join(docDir, "name")
docIndex = os.path.join(docDir, "index")
docIgnore = os.path.join(docDir, "ignore")
docBranch = os.path.join(docDir, "branch")
docLog = os.path.join(docDir, "log")
docLink = os.path.join(docDir, "link")

#
# Module exception.
#
class DocSyncError(Exception):
   """DocSync exception descriptor."""
   pass

#
# DOC repository manager.
#
class DocSync:
   #
   # Creates the repository from its root directory.
   #
   def __init__(self, root):
      self.loadRoot(root)
      self.loadName()
      self.loadIndex()
      self.loadIgnore()
      self.loadBranches()
   
   # Checks and loads the root
   def loadRoot(self, root):
      self.root = os.path.abspath(root)
      if not os.path.isdir(self.root) or not isRoot(self.root):
         raise DocSyncError("No repository found.")
   
   # Checks and loads the name
   def loadName(self):
      with open(os.path.join(self.root, docName)) as f:
         self.name = f.readline().strip()
      checkName(self.name)
   
   # Checks and loads the index
   def loadIndex(self):
      if not os.path.isfile(os.path.join(self.root, docIndex)):
         raise DocSyncError("Index corrupt.")
      
      self.index = list()
      with open(os.path.join(self.root, docIndex)) as f:
         for line in f:
            line = line.strip()
            end = line.find(" ")
            if end < 1:
               raise DocSyncError("Index corrupt.")
            entry = dict()
            entry["time"] = round(float(line[:end]), 3)
            start = end + 1
            end = line.find(" ", start)
            if end < 1:
               raise DocSyncError("Index corrupt.")
            entry["size"] = int(line[start:end])
            path = line[end + 1:]
            if not path.startswith("/"):
               raise DocSyncError("Index corrupt.")
            entry["path"] = path
            self.index.append(entry)
   
   # Checks and loads the ignore list
   def loadIgnore(self):
      if os.path.isfile(os.path.join(self.root, docIgnore)):
         self.ignore = set()
         with open(os.path.join(self.root, docIgnore)) as f:
            for line in f:
               line = line.strip()
               if not line or line.startswith("#"):
                  continue
               self.ignore.add(line)
      else:
         self.ignore = set([ ".*", ".*/", "/$*/", "/lost+found/", "/System Volume Information/" ])
      self.ignore |= set([ "/.doc/" ])
   
   # Checks and loads the branch list
   def loadBranches(self):
      if os.path.isfile(os.path.join(self.root, docBranch)):
         raise DocSyncError("Branch support not implemented.")
      self.base = "/"
      self.branches = list()
      self.branches.append("/")
   
   # Logs a line in the repository log.
   def log(self, line):
      with open(os.path.join(self.root, docLog), "a") as f:
         f.write(time.strftime("%H:%M:%S %d/%m/%y") + ": " + line + "\n")
   
   # Prints the log
   def printLog(self):
      with open(os.path.join(self.root, docLog)) as f:
         for line in f:
            print(line.strip())
   
   # Checks if a path must be ignored
   def checkIgnore(self, path, name):
      is_dir = path.endswith("/")
      ignoring = False
      for pattern in self.ignore:
         if pattern.endswith("/"):
            if not is_dir:
               continue
         else:
            if is_dir:
               continue
         if pattern.startswith("/"):
            ignoring = fnmatch.fnmatch(path, pattern)
         else:
            ignoring = fnmatch.fnmatch(name + "/", pattern)
         if ignoring:
            return True
      return False
   
   # Walks through the files of the repository calling the given function for each one
   def walkFiles(self, func, base):
      if base is None:
         base = self.root
         rel_base = "/"
      else:
         abs_base = os.path.abspath(base)
         if abs_base == self.root:
            base = self.root
            rel_base = "/"
         else:
            rel_base = "/" + os.path.relpath(abs_base, self.root)
            if not os.path.isdir(abs_base):
               if not os.path.isfile(abs_base):
                  raise DocSyncError("File not found.")
               func(rel_base, abs_base, rel_base)
               return rel_base
            rel_base += "/"
            base = self.root + rel_base
         
      for (root, subdirs, files) in os.walk(base):
         if root == self.root:
            root_path = "/"
         else:
            root_path = "/" + os.path.relpath(root, self.root) + "/"
         subdirs.sort()
         files.sort()
         func(root_path, root, rel_base)
         for name in subdirs[:]:
            path = os.path.join(root_path, name) + "/"
            if self.checkIgnore(path, name):
               subdirs.remove(name)
         for name in files[:]:
            path = os.path.join(root_path, name)
            if self.checkIgnore(path, name):
               files.remove(name)
            else:
               func(path, os.path.join(root, name), rel_base)
      return rel_base
   
   # Creates a new index for the current state
   def createIndex(self):
      self.fileCount = 0
      self.dirCount = 0
      with open(os.path.join(self.root, docIndex), "w") as f:
         def addToIndex(path, abs_path, rel_base):
            if path.endswith("/"):
               f.write("0.000 0 " + path + "\n") 
               self.dirCount += 1  
            else:
               stat = os.stat(abs_path)
               f.write(format(stat.st_mtime, ".3f") + " " + str(stat.st_size) + " " + path + "\n")   
               self.fileCount += 1     
      
         self.walkFiles(addToIndex, None)

   # Prints the index list
   def printIndex(self):
      for entry in self.index:
         print(entry["path"])
   
   # Compares two paths following the traverse order 
   def cmpPaths(self, a, b):
      if a == b:
         return 0
      a_dir = os.path.dirname(a)
      b_dir = os.path.dirname(b)
      if a_dir != b_dir:
         if b_dir.startswith(a_dir):
            return -1
         elif a_dir > b_dir:
            return 1
         return -1
      elif a > b:
         return 1
      return -1
   
   # Looks for differences between the index and the current files
   def diffFunc(self, base, func):
      self.current = 0
      
      def cmpIndex(path, abs_path, rel_base):
         while self.current < len(self.index):
            if not self.index[self.current]["path"].startswith(rel_base):
               self.current += 1
               continue
            
            comp = self.cmpPaths(path, self.index[self.current]["path"])
            if comp == 0:
               if path.endswith("/"):
                  func(path, "equ")
               else:
                  stat = os.stat(abs_path)
                  if round(stat.st_mtime, 3) != self.index[self.current]["time"] or \
                        stat.st_size != self.index[self.current]["size"]:
                     func(path, "mod")
                  else:
                     func(path, "equ")
               self.current += 1
               return
            elif comp > 0:
               func(self.index[self.current]["path"], "rem")
               self.current += 1
            else:
               break
         func(path, "add")

      rel_base = self.walkFiles(cmpIndex, base)
      
      while self.current < len(self.index):
         if self.index[self.current]["path"].startswith(rel_base):
            func(self.index[self.current]["path"], "rem")
         self.current += 1
   
   #
   # Prints the uncommitted changes in the repository
   #
   def diff(self, base):
      def printDiff(path, op):
         if op == "mod":
            print("M " + path)
         elif op == "add":
            print("+ " + path)
         elif op == "rem":
            print("- " + path)
         #elif op == "equ":
         #   print("  " + path)
      
      self.diffFunc(base, printDiff)

   #
   # Commits the current changes into the index
   #
   def commit(self):
      self.createIndex()
      self.log("Committed " + str(self.fileCount) + " files and " + str(self.dirCount) + " directories.")
      os.sync()
   
   # Loads the link information
   def loadLink(self):
      if not os.path.isfile(os.path.join(self.root, docLink)):
         return None
      with open(os.path.join(self.root, docLink)) as f:
         line = f.readline().strip()
         end = line.find(" ")
         if end < 1:
            raise DocSyncError("Link corrupt.")
         link = dict()
         link["checked"] = (int(line[:end]) == 1)
         start = end + 1
         end = line.find(" ", start)
         if end < 1:
            raise DocSyncError("Index corrupt.")
         name = line[start:end]
         path = line[end + 1:]
         if not path.startswith("/"):
            raise DocSyncError("Link corrupt.")
         link["path"] = path
         if not os.path.isdir(path) or not isRoot(path):
            print(path)
            raise DocSyncError("Linked repository not found.")
         d2 = DocSync(path)
         if self.root == d2.root:
            raise DocSyncError("Repository cannot be linked with itself.")
         if d2.name != name:
            print(path)
            raise DocSyncError("The path of the linked repository points to another.")
         link["doc"] = d2
         return link
   
   # Saves the link information
   def saveLink(self, path, name, checked):
      with open(os.path.join(self.root, docLink), "w") as f:
         f.write(("1" if checked else "0") + " " + name + " " + path)

   # Prints the link information
   def printLink(self):
      link = self.loadLink()
      if link is None:
         print("No repository linked.")
      else:
         d2 = link["doc"]
         print(d2.name + " " + ("(checked)" if link["checked"] else "(not checked)"))
         print(link["path"])
   
   # Sets the link to another repository
   def setLink(self, path):
      path = os.path.abspath(path)
      d2 = DocSync(path)
      if self.root == d2.root:
         raise DocSyncError("Repository cannot be linked with itself.")
      self.saveLink(path, d2.name, False)

   # Unsets the link to another repository
   def unsetLink(self):
      fname = os.path.join(self.root, docLink)
      if os.path.isfile(fname):
         os.remove(fname)
   
   # Checks this repository for uncommitted files
   def check(self):
      self.changes = False
      def findDiff(path, op):
         if op != "equ":
            self.changes = True
      
      self.diffFunc(None, findDiff)
      return not self.changes
   
   #
   # Checks this repository and its link for uncommitted files
   #
   def checkLink(self):
      link = self.loadLink()
      if link is None:
         raise DocSyncError("No repository linked.")
      d2 = link["doc"]
      ok = True
      if not self.check():
         print("This repository has uncommitted files.")
         ok = False
      if not d2.check():
         print("The linked repository has uncommitted files.")
         ok = False
      self.saveLink(link["path"], d2.name, ok)
      if ok:
         print("All ok, ready to transfer files.")
   
   # Compares this repository with another based on the indexes
   def cmpFunc(self, d2, base, func):
      if base is None:
         rel_base = "/"
      else:
         abs_base = os.path.abspath(base)
         if abs_base.startswith(self.root):
            base_root = self.root
         elif abs_base.startswith(d2.root):
            base_root = d2.root
         else:
            raise DocSyncError("Base not within a repository.")
         if abs_base == base_root:
            rel_base = "/"
         else:
            rel_base = "/" + os.path.relpath(abs_base, base_root)
            if os.path.isdir(abs_base):
               rel_base += "/"

      self.current = 0
      def cmpIndex(path, entry, rel_base):
         while self.current < len(self.index):
            if not self.index[self.current]["path"].startswith(rel_base):
               self.current += 1
               continue
            
            comp = self.cmpPaths(path, self.index[self.current]["path"])
            if comp == 0:
               if path.endswith("/"):
                  func(path, "equ", self.index[self.current], entry)
               else:
                  if entry["time"] != self.index[self.current]["time"] or \
                        entry["size"] != self.index[self.current]["size"]:
                     func(path, "mod", self.index[self.current], entry)
                  else:
                     func(path, "equ", self.index[self.current], entry)
               self.current += 1
               return
            elif comp > 0:
               func(self.index[self.current]["path"], "add", self.index[self.current], None)
               self.current += 1
            else:
               break
         func(path, "rem", None, entry)
         
      for entry in d2.index:
         path = entry["path"]
         if path.startswith(rel_base):
            cmpIndex(path, entry, rel_base)
      
      while self.current < len(self.index):
         if self.index[self.current]["path"].startswith(rel_base):
            func(self.index[self.current]["path"], "add", self.index[self.current], None)
         self.current += 1

   #
   # Shows the differences with another based on the indexes
   #
   def cmp(self, base):
      link = self.loadLink()
      if link is None:
         raise DocSyncError("No repository linked.")
      if not link["checked"]:
         print("Warning: Repositories not checked, could be comparing on stale indexes.")
      d2 = link["doc"]
      
      def printDiff(path, op, local, link):
         if op == "mod":
            if local is None or link is None:
               print("M " + path)
            elif local["time"] > link["time"]:
               print("> " + path)
            elif local["time"] < link["time"]:
               print("< " + path)
            else:
               print("M " + path)
         elif op == "add":
            print("+ " + path)
         elif op == "rem":
            print("- " + path)
         #elif op == "equ":
         #   print("  " + path)
      
      self.cmpFunc(d2, base, printDiff)

   #
   # Adds changed files to other reposistory based on the indexes
   #
   def push(self, base):
      link = self.loadLink()
      if link is None:
         raise DocSyncError("No repository linked.")
      if not link["checked"]:
         raise DocSyncError("Repositories not checked, cannot transfer files.")
      d2 = link["doc"]
      
      self.filesAdded = 0
      def applyDiff(path, op, local, link):
         src_path = self.root + path
         dst_path = d2.root + path
         if op == "mod":
            print("M " + path)
            shutil.copy2(src_path, dst_path)
            self.filesAdded += 1
         elif op == "add":
            print("+ " + path)
            if path.endswith("/"):
               os.makedirs(dst_path)
            else:
               dst_dir = os.path.dirname(dst_path)
               if not os.path.isdir(dst_dir):
                  os.makedirs(dst_dir)
               shutil.copy2(src_path, dst_path)
            self.filesAdded += 1
         
      self.cmpFunc(d2, base, applyDiff)
      
      if self.filesAdded == 0:
         print("No changes required.")
      else:
         d2.createIndex()
         d2.log("Pushed " + str(self.filesAdded) + " files/directories from \"" + self.name + "\".")
         os.sync()

   #
   # Adds or removes changed files to other reposistory based on the indexes
   #
   def pushdel(self, base):
      link = self.loadLink()
      if link is None:
         raise DocSyncError("No repository linked.")
      if not link["checked"]:
         raise DocSyncError("Repositories not checked, cannot transfer files.")
      d2 = link["doc"]
      
      self.filesAdded = 0
      self.filesDel = 0
      self.removeDirs = list()
      
      def tryRemoveDirs(path):
         for dentry in self.removeDirs[:]:
            if not path.startswith(dentry["path"]):
               print("- " + dentry["path"])
               try:
                  os.rmdir(dentry["dst_path"])
               except OSError as e:
                  if e.errno == errno.ENOTEMPTY:
                     print("Warning: Could not delete directory because it is not empty.")
                  else:
                     raise e
               self.removeDirs.remove(dentry)
               self.filesDel += 1
      
      def applyDiff(path, op, local, link):
         src_path = self.root + path
         dst_path = d2.root + path
         tryRemoveDirs(path)
         if op == "mod":
            print("M " + path)
            shutil.copy2(src_path, dst_path)
            self.filesAdded += 1
         elif op == "add":
            print("+ " + path)
            if path.endswith("/"):
               os.makedirs(dst_path)
            else:
               dst_dir = os.path.dirname(dst_path)
               if not os.path.isdir(dst_dir):
                  os.makedirs(dst_dir)
               shutil.copy2(src_path, dst_path)
            self.filesAdded += 1
         elif op == "rem":
            if path.endswith("/"): # Directories must be deleted once emptied
               dentry = dict()
               dentry["path"] = path
               dentry["dst_path"] = dst_path
               self.removeDirs.insert(0, dentry)
            else:
               print("- " + path)
               os.remove(dst_path)
               self.filesDel += 1
         
      self.cmpFunc(d2, base, applyDiff)
      
      tryRemoveDirs("/")
      
      if self.filesAdded == 0 and self.filesDel == 0:
         print("No changes required.")
      else:
         d2.createIndex()
         d2.log("Pushed " + str(self.filesAdded) + " files/directories and removed " + \
            str(self.filesDel) + " from \"" + self.name + "\".")
         os.sync()


#
# Formats a timestamp into a string.
#
def formatTime(ts):
   return time.strftime("%H:%M:%S %d/%m/%y", time.gmtime(ts))

#
# Checks if the given path is a repository root and returns True if so.
#
def isRoot(path):
   return os.path.isdir(os.path.join(path, docDir)) and \
            os.path.isfile(os.path.join(path, docName)) and \
            os.path.isfile(os.path.join(path, docIndex))

#
# Returns the root of the repository of the given dir or None
#
def findRoot(current):
   current = os.path.abspath(current)
   if not os.path.isdir(current):
      raise DocSyncError("Not a valid directory.")
   while True:
      if isRoot(current):
         return current
      parent = os.path.abspath(os.path.join(current, os.pardir))
      if parent == current:
         return None
      current = parent

#
# Checks that the repository name is valid.
# Raises an exception if not.
def checkName(name):
   if not name:
      raise DocSyncError("Respository names cannot be empty.")
   if name != name.strip():
      raise DocSyncError("Respository names cannot contain spaces or newlines.")
   if not re.match("^[a-z][a-z0-9_]*$", name):
      raise DocSyncError("Respository names must start with lowercase and contain only "
            "lowercase letters, numbers or underscores.")

#
# Loads the repository of the current directory (or a parent).
#
def load(current):
   root = findRoot(current)
   if root is None:
      raise DocSyncError("No repository found.")
   return DocSync(root)

#
# Creates a new repository
#
def create(root, name):
   if isRoot(root):
      raise DocSyncError("This directory already has a repository.")
   checkName(name)
   
   if not os.path.isdir(os.path.join(root, docDir)):
      os.mkdir(os.path.join(root, docDir))
   with open(os.path.join(root, docName), "w") as f:
      f.write(name)
   
   with open(os.path.join(root, docIndex), "w") as f:
      f.write("0.000 0 /")
   
   d = DocSync(root)
   d.log("Created empty repository named \"" + name + "\".")
   return d

