
# DocSync
Repository management tool designed to keep backup copies of large collections of big files in sync.

Runs on Python 3. Only tested under Debian/Linux.

## Installation
Clone the repository in a permanent (stable) directory and install the doc command in your system running:
```
$ sudo ./doc install
```
It will link the doc tool on `/usr/bin/doc` so you can run the command from any folder:
```
$ doc help
DOC repository management tool.
Usage: doc [<subcommand> [arguments ...]]
   If no subcommand is specified, it prints information about the current
   repository.
Subcommands available:
...
```
To uninstall, run:
```
$ sudo doc uninstall
```

## Repository Creation
You can create a repository on any directory with `doc init`, it doesn't matter if it has already files. You have to give a name to your repository:
```
/home/mydata$ ls
Documents  Photos  Videos
/home/mydata$ doc init
Respository name: mydata
Repository mydata created successfully.
```
Initially the repository will not track any file, so when looking for changes it will show the whole contents:
```
/home/mydata$ doc diff
+ /Documents/
+ /Documents/Sample.pdf
+ /Photos/
+ /Photos/Sample.jpg
+ /Videos/
+ /Videos/Sample.mp4
```

## Committing Content
DocSync saves a snapshot of the files and directories in a store called index. It only stores the files list and signature, not the full content like a version control system. That makes it suitable to detect changes in directories with many files and big ones. You can think of DocSync as a `rsync` tool with index.

To add the changes to the index, use the commit command:
```
/home/mydata$ doc commit
```
The tool will look for changes in the files from the last commit:
```
/home/mydata$ doc diff   # No change
/home/mydata$ echo testing > testing.txt
/home/mydata$ doc diff
+ /testing.txt
```

DocSync tracks both files and directories.

## Comparing Repositories
Imagine that we connect an empty USB drive to the system and want to use it for backups. First we create a new repository in the disc:
```
/mount/mydrive$ doc init mydrive
Repository mydrive created successfully.
```
Then to sync both repositories we have to link them:
```
/home/mydata$ doc link /mount/mydrive
/home/mydata$ doc link
mydrive (not checked)
/mount/mydrive
```
Note that we work from `/home/mydata`, which is the "source of truth" or repository from which to copy files. DocSync can sync in both directions, but you have to run commands from the repository where the data, or that part of the data, is in its latest version. This means that you always transfer files from the origin repository. We'll better understand this with the example.

Once the repositories are linked, you can compare them:
```
/home/mydata$ doc cmp
Warning: Repositories not checked, could be comparing on stale indexes.
+ /Documents/
+ /Documents/Sample.pdf
+ /Photos/
+ /Photos/Sample.jpg
+ /Videos/
+ /Videos/Sample.mp4
```
DocSync shows you the changes that will have to do to make `mydrive` to be identical to `mydata`. It bases the comparison on the indexes, which are much faster to process, not on the actual files.

## Synchronizing Repositories
Note that the link appears as "not checked", it means that the tool has not checked that there aren't any uncommitted file in the repositories. To check the link run:
```
/home/mydata$ doc check
This repository has uncommitted files.
```
It warns us that `mydata` has uncommitted changes from our last edit, so lets add them to the index and validate the link:
```
/home/mydata$ doc diff
+ /testing.txt
/home/mydata$ doc commit
/home/mydata$ doc check
All ok, ready to transfer files.
/home/mydata$ doc link
mydrive (checked)
/mount/mydrive
```

Now that the link is checked we can apply the changes on `mydrive`:
```
/home/mydata$ doc push
+ /testing.txt
+ /Documents/
+ /Documents/Sample.pdf
+ /Photos/
+ /Photos/Sample.jpg
+ /Videos/
+ /Videos/Sample.mp4
/home/mydata$ doc cmp   # No difference
/home/mydata$
```
And we have both repositories synchronized.

## Complex Synchronization
Lets make some more changes to both repositories:
```
/mount/mydrive$ echo testing2 > testing.txt
/mount/mydrive$ doc diff
M /testing.txt
/mount/mydrive$ doc commit
/mount/mydrive$ cd /home/mydata
/home/mydata$ rm Documents/Sample.pdf 
/home/mydata$ echo hello > Documents/hello.txt
/home/mydata$ doc diff
- /Documents/Sample.pdf
+ /Documents/hello.txt
/home/mydata$ doc commit
```
Now we have this scenario:
```
/home/mydata$ doc cmp
< /testing.txt
- /Documents/Sample.pdf
+ /Documents/hello.txt
```
Which means that in order to make `mydrive` to match `mydata`, DocSync would have to copy `/Documents/hello.txt`, delete `/Documents/Sample.pdf` and override `/testing.txt`. However, the `<` of `/testing.txt` tells that it might make more sense to bring that file from `mydrive`, instead of using the `mydata`'s version. DocSync knows that because the version of `/testing.txt` in `mydrive` is more recent.

Here I'd like to stress that DocSync cannot recover `/testing.txt`. The index does not store the contents of the files, that's why it is so efficient and can handle big files. DocSync can only notice that there is a file missing and can help you find it in a backup copy.

Therefore we want to push the changes of `/Documents/`, but bring the latest `/testing.txt` from `mydrive`. To do so we do a partial push:
```
/home/mydata$ doc push Documents/
+ /Documents/hello.txt
/home/mydata$ doc cmp
< /testing.txt
- /Documents/Sample.pdf
```
Note that the `push` command only adds or overrides files, but never deletes them. To fully synchronize the `/Documents/` folder we need to use the `pushdel`, which is similar to `push` but removes files and directories as well. This time we'll use it changing directory for show (it is equivalent):
```
/home/mydata$ cd Documents/
/home/mydata/Documents$ doc pushdel .
- /Documents/Sample.pdf
/home/mydata/Documents$ cd ..
/home/mydata$ doc cmp
< /testing.txt
```

There is still one file different, but we want to bring it from `mydrive`, so we have to execute DocSync from that repository. We start with the linking and checking:
```
/mount/mydrive$ doc link /home/mydata
/mount/mydrive$ doc check
All ok, ready to transfer files.
/mount/mydrive$ doc cmp
> /testing.txt
```
The comparison now suggests that we should send the file to the linked repository (`mydata`), so we push only this file:
```
/mount/mydrive$ doc push testing.txt 
M /testing.txt
```
And there we are! Now both repositories show in sync
```
/mount/mydrive$ doc cmp   # No difference
/mount/mydrive$ cd /home/mydata
/home/mydata$ doc cmp   # No difference
```

After we are done, it is a good idea to clear the links:
```
/mount/mydrive$ doc unlink
/mount/mydrive$ cd /home/mydata
/home/mydata$ doc unlink
```
That way, the next time DocSync will force us to recheck the link before transfering files.

## A Bit of History
Last thing to mention is that DocSync keeps a log of all the actions that modify a repository. To see what happened run:
```
/mount/mydrive$ doc log
22:06:35 09/01/23: Created empty repository named "mydrive".
22:20:33 09/01/23: Pushed 7 files/directories from "mydata".
22:50:22 09/01/23: Committed 4 files and 4 directories.
22:57:36 09/01/23: Pushed 1 files/directories from "mydata".
23:02:01 09/01/23: Pushed 0 files/directories and removed 1 from "mydata".
```

